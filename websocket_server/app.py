from tornado import websocket, web, ioloop
import json

clients = []
PRODUCTION = False

class SocketHandler(websocket.WebSocketHandler):
    def check_origin(self, origin):
        return True

    def open(self):
        if self not in clients:
            clients.append(self)

    def on_close(self):
        if self in clients:
            clients.remove(self)


class ApiHandler(web.RequestHandler):
    @web.asynchronous
    def get(self, *args):
        if PRODUCTION and not "sushi-cakypa.ru" in self.request.headers['Origin']:
            return
        self.finish()
        order_id = self.get_argument("order_id")
        customer_name = self.get_argument("customer_name")
        data = {"order_id": order_id, "customer_name": customer_name}
        data = json.dumps(data)
        for c in clients:
            c.write_message(data)


    @web.asynchronous
    def post(self):
        pass


app = web.Application([
    (r'/', SocketHandler),
    (r'/api', ApiHandler),
])

if __name__ == '__main__':
    app.listen(8888)
    ioloop.IOLoop.instance().start()
