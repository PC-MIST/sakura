from app.cart.models import Cart, CartItem
from app.contrib.mixins import AppHeaderMixin
from django.views.generic import FormView, TemplateView
from django.conf import settings
from forms import OrderForm
from models import Order
import requests


class OrderFormView(AppHeaderMixin, FormView):
    form_class = OrderForm
    success_url = '/order/thanks/'

    def get_template_names(self):
        cart = Cart().get_cart(self.request)
        if CartItem.objects.filter(cart=cart):
            return 'site/order/index.html'
        return 'site/order/no_order.html'

    def form_valid(self, form):
        order_data = form.cleaned_data
        cart = Cart().get_cart(self.request)
        order_data['cart'] = cart
        cart = Cart.objects.filter(id=cart.id)
        items = CartItem.objects.filter(cart=cart)
        order_data['total'] = sum([i.amount*i.product.price for i in items])

        cart.update(user=None, session=None)
        Order(**order_data).save()

        WEB_SOCKET_SERVER_API_URL = getattr(settings, 'WEB_SOCKET_SERVER_API_URL')
        params = {
            "order_id": Order.objects.get(**order_data).id,
            "customer_name": u"%s %s" % (order_data['first_name'], order_data['last_name'])
        }
        requests.get(WEB_SOCKET_SERVER_API_URL, params=params)
        return super(OrderFormView, self).form_valid(form)


class OrderThanksView(AppHeaderMixin, TemplateView):
    template_name = 'site/order/thanks.html'
