# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-10-06 08:02
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('cart', '0002_auto_20160506_2144'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('completion_date', models.DateTimeField(auto_now=True, verbose_name='\u0414\u0430\u0442\u0430 \u0437\u0430\u0432\u0435\u0440\u0448\u0435\u043d\u0438\u044f')),
                ('status', models.CharField(choices=[('waiting', '\u041e\u0436\u0438\u0434\u0430\u043d\u0438\u0435'), ('proccessing', '\u041e\u0431\u0440\u0430\u0431\u043e\u0442\u043a\u0430'), ('delivering', '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u044f\u0435\u0442\u0441\u044f'), ('delivered', '\u0414\u043e\u0441\u0442\u0430\u0432\u043b\u0435\u043d'), ('canceled', '\u041e\u0442\u043c\u0435\u043d\u0435\u043d')], default=('waiting', '\u041e\u0436\u0438\u0434\u0430\u043d\u0438\u0435'), max_length=160, verbose_name='\u0421\u0442\u0430\u0442\u0443\u0441 \u0437\u0430\u043a\u0430\u0437\u0430')),
                ('phone', models.CharField(blank=True, max_length=30, verbose_name='\u041d\u043e\u043c\u0435\u0440 \u0442\u0435\u043b\u0435\u0444\u043e\u043d\u0430')),
                ('delivery_location', models.CharField(blank=True, max_length=200, verbose_name='\u041c\u0435\u0441\u0442\u043e \u0434\u043e\u0441\u0442\u0430\u0432\u043a\u0438')),
                ('persons', models.CharField(blank=True, max_length=4, verbose_name='\u0427\u0438\u0441\u043b\u043e \u043f\u0435\u0440\u0441\u043e\u043d')),
                ('comment', models.TextField(blank=True, verbose_name='\u041a\u043e\u043c\u043c\u0435\u043d\u0442\u0430\u0440\u0438\u0439')),
                ('first_name', models.CharField(blank=True, max_length=90, verbose_name='\u0418\u043c\u044f')),
                ('last_name', models.CharField(blank=True, max_length=90, verbose_name='\u0424\u0430\u043c\u0438\u043b\u0438\u044f')),
                ('total', models.FloatField(blank=True, default=0.0, verbose_name='\u0418\u0442\u043e\u0433\u043e\u0432\u0430\u044f \u0446\u0435\u043d\u0430')),
                ('cart', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='cart.Cart', verbose_name='\u041a\u043e\u0440\u0437\u0438\u043d\u0430')),
                ('operator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='\u041e\u043f\u0435\u0440\u0430\u0442\u043e\u0440')),
            ],
            options={
                'ordering': ['creation_date'],
                'verbose_name': '\u0437\u0430\u043a\u0430\u0437',
                'verbose_name_plural': '\u0437\u0430\u043a\u0430\u0437\u044b',
            },
        ),
    ]
