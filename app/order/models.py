# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from app.cart.models import Cart
from app.rebate.models import Rebate

STATUS_LIST = (
    ("waiting", u"Ожидание"),
    ("proccessing", u"Обработка"),
    ("delivering", u"Доставляется"),
    ("delivered", u"Доставлен"),
    ("canceled", u"Отменен"),
)


class Order(models.Model):
    creation_date = models.DateTimeField(verbose_name=u"Дата создания",
                                         auto_now_add=True)
    completion_date = models.DateTimeField(verbose_name=u"Дата завершения",
                                           auto_now=True)
    status = models.CharField(verbose_name=u"Статус заказа",
                              choices=STATUS_LIST, max_length=160,
                              default=("waiting", u"Ожидание"))
    cart = models.ForeignKey(Cart, verbose_name=u"Корзина")
    phone = models.CharField(verbose_name=u"Номер телефона", max_length=30,
                             blank=True)
    delivery_location = models.CharField(verbose_name=u"Место доставки",
                                         max_length=200, blank=True)
    persons = models.CharField(verbose_name=u"Число персон", max_length=4,
                               blank=True)
    comment = models.TextField(verbose_name=u"Комментарий", blank=True)
    first_name = models.CharField(verbose_name=u"Имя", max_length=90,
                                  blank=True)
    last_name = models.CharField(verbose_name=u"Фамилия", max_length=90,
                                 blank=True)
    operator = models.ForeignKey(User, verbose_name=u"Оператор", blank=True,
                                 null=True)
    rebate = models.ForeignKey(Rebate, verbose_name=u'Скидка',
                               blank=True, null=True)
    total = models.FloatField(verbose_name=u"Итоговая цена", blank=True, default=0.0)

    class Meta:
        ordering = ['creation_date']
        verbose_name = 'заказ'
        verbose_name_plural = 'заказы'

    def __unicode__(self):
        return u"Заказ для  %s %s." % (self.first_name, self.last_name)
