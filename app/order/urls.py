# coding: utf-8
from django.conf.urls import url
from views import OrderFormView, OrderThanksView

urlpatterns = [
    url(r'^$', OrderFormView.as_view(app_header=u'Ваш заказ'), name="index"),
    url(r'^thanks/$', OrderThanksView.as_view(app_header=u'Спасибо за заказ'), name="thanks"),
]