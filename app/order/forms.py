# coding: utf-8
from django import forms

from app.contrib.validators import phone_number, person_number, location, name, surname

FORM_ATTRS = {'class': 'form-control'}


class OrderForm(forms.Form):
    first_name = forms.CharField(label=u'Имя', max_length=30,
                                 widget=forms.TextInput(attrs=FORM_ATTRS),
                                 validators=[name])
    last_name = forms.CharField(label=u'Фамилия', max_length=30,
                                widget=forms.TextInput(attrs=FORM_ATTRS),
                                 validators=[surname])
    phone = forms.CharField(label=u'Телефон', max_length=20,
                            widget=forms.TextInput(attrs=FORM_ATTRS),
                            validators=[phone_number])
    comment = forms.CharField(label=u'Комментарий к заказу',
                              widget=forms.Textarea(attrs=FORM_ATTRS),
                              required=False)
    persons = forms.CharField(label=u'Количество персон', max_length=4,
                              widget=forms.TextInput(attrs=FORM_ATTRS),
                              validators=[person_number])
    delivery_location = forms.CharField(label=u'Адрес доставки', max_length=100,
                                        widget=forms.TextInput(
                                            attrs=FORM_ATTRS),
                                        validators=[location])
