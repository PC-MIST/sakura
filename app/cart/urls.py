from django.conf.urls import url, include
from views import CartView

urlpatterns = [
    url(r'^$', CartView.as_view(), name="index"),
    url(r'^api/', include('app.cart.api.urls', namespace="api")),
]
