from django.views.generic import TemplateView

class CartView(TemplateView):
    template_name = 'site/cart/index.html'