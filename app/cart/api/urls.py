# -*- coding: utf8 -*-
from django.conf.urls import url
from views import (GetCartAPI, GetCartItemsAPI, GetProductsAPI, SetCartItemAPI,
                   DelCartItemAPI)

urlpatterns = [
    url(r'^get_cart/$', GetCartAPI.as_view(), name='get_cart'),
    url(r'^get_cart_items/$', GetCartItemsAPI.as_view(), name='get_cart_items'),
    url(r'^set_cart_item/$', SetCartItemAPI.as_view(), name='set_cart_item'),
    url(r'^get_products/(?P<category>[^/]+)/$', GetProductsAPI.as_view(), name='get_products'),
    url(r'^del_cart_item/$', DelCartItemAPI.as_view(), name='del_cart_item'),

]
