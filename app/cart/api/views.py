# -*- coding: utf8 -*-
from __future__ import absolute_import
from rest_framework.response import Response
from rest_framework.views import APIView
from app.cart.models import Cart, CartItem
from app.menu.models import Product, CATEGORY_LIST
import json


class GetCartAPI(APIView):
    http_method_names = ['get']

    def get(self, *args, **kwargs):
        cart = Cart().get_cart(self.request)
        if cart:
            return Response({
                'result': 'success',
                'data': []
            })
        return Response({
            'result': 'error',
            'data': ['No cart for that user']
        })


class GetCartItemsAPI(APIView):
    http_method_names = ['get']

    def get(self, *args, **kwargs):
        cart = Cart().get_cart(self.request)
        if cart:
            data = CartItem.objects.prefetch_related('product').filter(
                cart=cart)
            data = [
                {
                    'product': x._product_cache.name,
                    'price': x._product_cache.price,
                    'image': x._product_cache.image.url,
                    'amount': x.amount
                } for x in data]
            return Response({
                'result': 'success',
                'data': json.dumps(data)
            })
        return Response({
            'result': 'error',
            'data': json.dumps(['No cart for that user'])
        })


class GetProductsAPI(APIView):
    http_method_names = ['get']
    values = ('name', 'price', 'weight', 'image', 'about')

    def get(self, *args, **kwargs):
        query_args = {
            'is_enabled': True
        }
        if kwargs.get('category') != 'all':
            query_args.update({'category': kwargs.get('category')})
            data = Product.objects.filter(**query_args).values(*self.values).order_by('category', 'price')
            data = list(data)
        else:
            data = {}
            for category in CATEGORY_LIST:
                query_args.update({'category': category[0]})
                data[category[0]] = Product.objects.filter(**query_args).values(*self.values).order_by('price')
            result = []
            for category in CATEGORY_LIST:
                if data[category[0]]:
                    for item in data[category[0]]:
                        result.append(item)
            data = result

        return Response({
            'result': 'success',
            'data': json.dumps(data)
        })


class SetCartItemAPI(APIView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        data = json.loads(self.request.data)
        cart = Cart().get_cart_or_create(request)
        product = Product.objects.get(name=data['product'])
        cart_item = CartItem.objects.filter(cart=cart, product=product).first()
        if cart_item:
            CartItem.objects.filter(cart=cart, product=product).update(
                amount=data['amount'])
            message = 'Cart item has been created'
        else:
            CartItem.objects.create(cart=cart, product=product, amount=1)
            message = 'Cart item has been updated'
        return Response({
            'result': 'success',
            'data': [message]
        })


class DelCartItemAPI(APIView):
    http_method_names = ['post']

    def post(self, request, *args, **kwargs):
        data = json.loads(self.request.data)
        cart = Cart().get_cart_or_create(request)
        product = Product.objects.get(name=data['product'])
        CartItem.objects.filter(cart=cart, product=product).delete()
        return Response({
            'result': 'success',
            'data': ['cart_items has been deleted']
        })
