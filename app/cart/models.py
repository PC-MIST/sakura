# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from app.menu.models import Product


class Cart(models.Model):
    user = models.OneToOneField(User, verbose_name=u"User", blank=True,
                                null=True)
    session = models.CharField(u"Session", blank=True, max_length=100,
                               null=True)
    creation_date = models.DateTimeField(u"Creation date", auto_now_add=True)
    modification_date = models.DateTimeField(u"Modification date",
                                             auto_now=True)

    def __unicode__(self):
        if self.user:
            return u"Корзина %s. Пользователь %s" % (self.id, self.user)
        return u"Корзина %s. Ключ сессии %s" % (self.id, self.session)

    def get_cart_or_create(self, request):
        if request.user.is_authenticated():
            cart = Cart.objects.filter(user=request.user).first()
        else:
            cart = Cart.objects.filter(
                session=request.session.session_key).first()
        if not cart:
            if request.user.is_authenticated():
                cart = Cart.objects.create(user=request.user)
            else:
                cart = Cart.objects.create(session=request.session.session_key)
        return cart

    def get_cart(self, request):
        if request.user.is_authenticated():
            cart = Cart.objects.filter(user=request.user).first()
        else:
            cart = Cart.objects.filter(
                session=request.session.session_key).first()
        if cart:
            return cart
        return None

    class Meta:
        app_label = 'cart'
        verbose_name = 'корзина'
        verbose_name_plural = 'корзины'


class CartItem(models.Model):
    cart = models.ForeignKey(Cart, verbose_name=u"Cart")
    product = models.ForeignKey(Product, verbose_name=u"Product")
    amount = models.PositiveIntegerField(u"Quantity", blank=True, null=True)
    creation_date = models.DateTimeField(u"Creation date", auto_now_add=True)
    modification_date = models.DateTimeField(u"Modification date",
                                             auto_now=True)

    class Meta:
        app_label = 'cart'
        verbose_name = 'товар корзины'
        verbose_name_plural = 'товары корзины'

    def __unicode__(self):
        return u'Корзина %s. %s %s шт.' % (
            self.cart.id, self.product.name, self.amount)
