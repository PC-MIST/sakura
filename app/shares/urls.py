# coding: utf-8
from django.conf.urls import url
from views import SharesView

urlpatterns = [
    url(r'^$', SharesView.as_view(app_header=u'Акции'), name="index"),
]
