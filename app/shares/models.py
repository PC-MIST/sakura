# -*- coding: utf-8 -*-
from django.db import models
from ckeditor.fields import RichTextField


class Shares(models.Model):
    creation_date = models.DateTimeField(verbose_name=u"Дата создания", auto_now_add=True)
    title = models.CharField(verbose_name=u"Название акции", max_length=256)
    text = RichTextField(verbose_name=u"Текст акции")
    image = models.ImageField(verbose_name=u"Картинка акции", upload_to='shares/')
    is_enable = models.BooleanField(verbose_name=u"Включено")

    class Meta:
        ordering = ['creation_date']
        verbose_name = 'акция'
        verbose_name_plural = 'акции'

    def __unicode__(self):
        return u"Акция  %s." % self.title
