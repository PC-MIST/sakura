from django.views.generic import ListView
from app.contrib.mixins import AppHeaderMixin
from .models import Shares

class SharesView(AppHeaderMixin, ListView):
    template_name = 'site/shares/index.html'
    model = Shares
    context_object_name = 'shares'
