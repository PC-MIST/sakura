from django.views.generic import TemplateView
from app.contrib.mixins import AppHeaderMixin, StaticPageContentMixin


class ShippingConditionView(AppHeaderMixin, StaticPageContentMixin, TemplateView):
    template_name = 'site/shipping_cond/index.html'
    page_slug = 'shipping_condition'
