# coding: utf-8
from django.conf.urls import url
from views import ShippingConditionView

urlpatterns = [
    url(r'^$', ShippingConditionView.as_view(app_header=u'Условия доставки'), name="index"),
]