from django.views.generic import TemplateView
from ckeditor_uploader.views import ImageUploadView
from forms import ImageUploadForm


class UploadView(TemplateView, ImageUploadView):
    http_method_names = ['get', 'post']
    template_name = 'adm/upload_images.html'

    def get_context_data(self, **kwargs):
        kwargs['form'] = ImageUploadForm
        return super(UploadView, self).get_context_data(*args, **kwargs)
