# coding: utf-8
from app.menu.models import CATEGORY_LIST
from app.order.models import Order
from app.static_pages.models import StaticPage
from django.http.response import HttpResponseNotFound
from django.conf import settings

CATEGORIES = [c[0] for c in CATEGORY_LIST]
CATEGORIES.append("all")


class ProductCategoryMixin(object):
    category = None

    def get_context_data(self, *args, **kwargs):
        kwargs['category'] = self.category
        return super(ProductCategoryMixin, self).get_context_data(*args,
                                                                  **kwargs)

    def __init__(self, *args, **kwargs):
        self.category = kwargs['category']
        if not self.category:
            raise NotImplementedError('Set category')
        if self.category not in CATEGORIES:
            raise NameError('There is no ' + self.category + ' category')
        return super(ProductCategoryMixin, self).__init__(*args, **kwargs)


class AppHeaderMixin(object):
    app_header = None

    def get_context_data(self, *args, **kwargs):
        kwargs['app_header'] = self.app_header
        return super(AppHeaderMixin, self).get_context_data(*args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.app_header = kwargs['app_header']
        return super(AppHeaderMixin, self).__init__(*args, **kwargs)


class StaticPageContentMixin(object):
    page_slug = None

    def get_context_data(self, *args, **kwargs):
        kwargs['static_hypertext'] = StaticPage.objects.filter(
            page_slug=self.page_slug).first().html_text
        return super(StaticPageContentMixin, self).get_context_data(*args,
                                                                    **kwargs)

    def __init__(self, *args, **kwargs):
        if not self.page_slug:
            raise NotImplementedError('Set static page slug')
        return super(StaticPageContentMixin, self).__init__(*args, **kwargs)


class StuffPermissionMixin(object):
    def get(self, request, *args, **kwargs):
        if request.user.is_staff:
            return super(StuffPermissionMixin, self).get(request, *args,
                                                         **kwargs)
        return HttpResponseNotFound()

    def post(self, request, *args, **kwargs):
        if request.user.is_staff:
            return super(StuffPermissionMixin, self).post(request, *args,
                                                          **kwargs)
        return HttpResponseNotFound()


class ManagementContentMixin(object):
    def get_context_data(self, *args, **kwargs):
        kwargs['orders_number'] = Order.objects.exclude(
            status__contains="canceled").exclude(
            status__contains="delivered").count()
        kwargs['web_socket_server_url'] = getattr(settings, 'WEB_SOCKET_SERVER_URL')
        return super(ManagementContentMixin, self).get_context_data(*args,
                                                                    **kwargs)
