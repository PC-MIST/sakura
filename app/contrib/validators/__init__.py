# coding: utf-8
from django.core.exceptions import ValidationError
import re
from django.core.validators import validate_integer, MaxLengthValidator

RE = {
    'phone_number': r'((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$',
    'email': r'^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$'
}


def phone_number(value):
    r = re.compile(RE['phone_number'])
    if not r.findall(value):
        raise ValidationError(u'Номер должен состоять из цифр, например, "8 (912) 345-67-89", "+7 912 345-67-89"')


def person_number(value):
    validate_integer(value)
    if len(value) > 4:
        raise ValidationError(u'Введите меньше 4-х цифр. На %s-х не хватит :)' % (value))


def location(value):
    MaxLengthValidator(limit_value=100)


def str_ru(value, message):
    result = True
    ru = u"ЙЦУКЕНГШЩЗХЪЭЖДЛОРПАВЫФЯЧСМИТЬБЮЁйцукенгшщзхъэждлорпавыфюбьтимсчяё"
    for i in range(0, len(value)):
        result *= value[i] in ru
    if not result:
        raise ValidationError(message=message)


def name(value):
    str_ru(value, u'Имя должно состоять из русских символов')


def surname(value):
    str_ru(value, u'Фамилия должна состоять из русских символов')


def contact(value):
    result = False
    rexps = [RE['phone_number'], RE['email']]
    for rexp in rexps:
        result = result or re.compile(rexp).findall(value)
    if not result:
        raise ValidationError(u'Введите номер телефона или e-mail')
