# -*- coding: utf8 -*-
from app.contrib.mixins import ManagementContentMixin
from django.views.generic.base import TemplateView


class IndexView(ManagementContentMixin, TemplateView):
    template_name = 'management/index.html'


class CallbacksView(ManagementContentMixin, TemplateView):
    template_name = 'management/callbacks.html'


class RecallsView(ManagementContentMixin, TemplateView):
    template_name = 'management/recalls.html'


class RequestsView(ManagementContentMixin, TemplateView):
    template_name = 'management/requests.html'
