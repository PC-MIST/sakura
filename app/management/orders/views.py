# -*- coding: utf8 -*-
import datetime

import os
from app.cart.models import CartItem
from app.contrib.mixins import StuffPermissionMixin, ManagementContentMixin
from app.order.models import Order
from app.order.models import STATUS_LIST
from app.sitesettings.models import SiteSetting
from django.conf import settings
from django.core.urlresolvers import reverse_lazy
from django.http.response import HttpResponseRedirect
from django.views.generic import FormView, TemplateView
from django.views.generic.list import ListView
from forms import ManageOrderForm
from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Border, Side


class OrdersView(ManagementContentMixin, StuffPermissionMixin, ListView):
    model = Order
    context_object_name = 'orders'
    template_name = 'management/order/orders.html'

    def get_context_data(self, **kwargs):
        orders = Order.objects.all()
        result = {}
        [result.update({stat[0]: []}) for stat in STATUS_LIST]
        for o in orders:
            for (k, v) in STATUS_LIST:
                if k in o.status:
                    result[k].append(o)
        kwargs['orders'] = result
        return super(OrdersView, self).get_context_data(**kwargs)


class OrderManageView(ManagementContentMixin, StuffPermissionMixin, FormView):
    template_name = 'management/order/manage_order.html'
    form_class = ManageOrderForm

    def get_success_url(self):
        return reverse_lazy('management:orders:manage_order', kwargs={'order_id': self.kwargs['order_id']})

    def get(self, request, *args, **kwargs):
        stuff_id = request.user.id
        order = Order.objects.filter(id=kwargs['order_id']).first()
        if order.operator:
            if order.operator.id != stuff_id:
                return HttpResponseRedirect('/management/orders/access-error/')
            return super(OrderManageView, self).get(request, *args, **kwargs)
        else:
            Order.objects.filter(id=kwargs['order_id']).update(
                operator=request.user)
            return super(OrderManageView, self).get(request, *args, **kwargs)

    def get_form_initial(self):
        order = Order.objects.filter(id=self.kwargs['order_id']).first()
        return {
            'status': order.status,
            'cart': order.cart,
            'phone': order.phone,
            'delivery_location': order.delivery_location,
            'persons': order.persons,
            'comment': order.comment,
            'first_name': order.first_name,
            'last_name': order.last_name,
            'operator': order.operator,
            'rebate': order.rebate if hasattr(order, 'rebate') else None,
            'total': order.total,
        }

    def form_valid(self, form):
        Order.objects.filter(id=self.kwargs['order_id']).update(
            **form.cleaned_data)
        return super(OrderManageView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        order = Order.objects.filter(id=self.kwargs['order_id']).first()
        total = order.total
        discount = order.rebate
        discount = discount.percent if hasattr(discount, 'percent') else 0

        kwargs['discount'] = discount
        kwargs['order'] = order
        kwargs['total_discount'] = (1.0 - discount / 100.0) * total
        kwargs['cart_items'] = CartItem.objects.filter(cart_id=order.cart.id)
        kwargs['form'] = ManageOrderForm(self.get_form_initial())
        return super(OrderManageView, self).get_context_data(**kwargs)


class OrderAccessError(ManagementContentMixin, TemplateView):
    template_name = 'management/order/access_order_error.html'


def print_order(request, *args, **kwargs):
    order = Order.objects.filter(id=kwargs['order_id']).first()
    total = order.total
    discount = order.rebate if hasattr(order, 'rebate') else None
    discount = discount.percent if hasattr(discount, 'percent') else 0
    total_discount = total * (1.0 - discount / 100.0)

    cart_items = CartItem.objects.filter(cart_id=order.cart.id)

    order_info = {
        'note': order.comment,
        'persons': order.persons,
        'client_name': u"{} {}".format(order.first_name, order.last_name),
        'location': order.delivery_location,
        'phone': order.phone,
        'date': order.creation_date,
        'date_print': datetime.datetime.now(),
        'total': total,
        'total_discount': total_discount,
    }

    info_styles = {
        'font': Font(name='Calibri', size=10, bold=True),
        'font-order-info': Font(name='Calibri', size=12, bold=True),
        'font-total-price': Font(name='Calibri', size=12, bold=True),
        'alignment': Alignment(wrapText=True, vertical="top"),
        'alignment-center-top': Alignment(wrapText=True, vertical="top", horizontal="center"),
        'greeting': Font(name='Calibri', size=16, bold=True),
        'alignment-right': Alignment(horizontal="right")
    }

    thin = Side(border_style="thin", color="000000")
    borders = {
        'cart-items-table': Border(left=thin, right=thin, top=thin, bottom=thin)
    }

    header_styles = {
        'font': Font(name='Calibri', size=11, bold=True),
        'alignment': Alignment(horizontal="center")
    }

    filename = "order_%s.xlsx" % (kwargs['order_id'])

    wb = Workbook()
    ws = wb.active

    def append_order_info(row_number, info):
        ws.merge_cells(start_row=row_number, start_column=2, end_row=row_number, end_column=5)
        ws.append(info)
        ws.cell(row=row_number, column=1).font = info_styles['font']
        ws.cell(row=row_number, column=2).alignment = info_styles['alignment']

    def append_gen_info(row_number, info):
        append_order_info(row_number, info)

    row_number = 1
    ws.merge_cells(start_row=row_number, start_column=1, end_row=row_number, end_column=5)
    ws.append([u'Сакура желает Вам приятного аппетита :)'])
    ws.cell(row=row_number, column=1).font = info_styles['greeting']
    ws.cell(row=row_number, column=1).alignment = info_styles['alignment-center-top']
    ws.row_dimensions[row_number].height = 35

    site_info = {}
    for setting in SiteSetting.objects.all():
        site_info[setting.slug] = setting.value

    row_number += 1
    append_gen_info(row_number, [u'Адрес:', site_info['address']])

    row_number += 1
    append_gen_info(row_number, [u'Телефон:', site_info['call_number']])

    row_number += 1
    append_gen_info(row_number, [u'E-mail:', site_info['email']])

    row_number += 1
    append_gen_info(row_number, [u'Сайт:', site_info['site_url']])

    row_number += 1
    append_order_info(row_number, [u'Группа вконтакте:', site_info['vk_url']])

    row_number += 1
    ws.append([])

    row_number += 1
    ws.merge_cells(start_row=row_number, start_column=1, end_row=row_number, end_column=5)
    ws.append([u'Информация о заказе'])
    ws.cell(row=row_number, column=1).font = info_styles['font-order-info']

    row_number += 1
    append_order_info(row_number,
                      [u'Получен:', u'%s' % order_info['date'].strftime("%d.%m.%Y %H:%M")])

    row_number += 1
    append_order_info(row_number, [u'Телефон', order_info['phone']])

    row_number += 1
    append_order_info(row_number, [u'Адрес:', order_info['location']])

    row_number += 1
    append_order_info(row_number, [u'Количество персон:', order_info['persons']])

    row_number += 1
    append_order_info(row_number, [u'Примечание:', order_info['note']])
    ws.row_dimensions[row_number].height = 15 * (1 + len(order_info['note']) / 65)
    ws.cell(row=row_number, column=1).alignment = info_styles['alignment']

    row_number += 1
    ws.append([])

    row_number += 1
    ws.merge_cells(start_row=row_number, start_column=1, end_row=row_number, end_column=5)
    ws.append([u'Товары'])
    ws.cell(row=row_number, column=1).font = info_styles['font-order-info']

    row_number += 1
    items = []
    [items.append([i.product.name, '', i.amount, u'%s руб.' % i.product.price,
                   u'%s руб.' % (i.product.price * i.amount)]) for i in cart_items]

    ws.merge_cells(start_row=row_number, start_column=1, end_row=row_number, end_column=2)
    ws.append([u'Наименование товара', '', u'Количество', u'Цена', u'Всего'])

    for i in range(1, 6):
        ws.cell(row=row_number, column=i).border += borders['cart-items-table']

    for item in items:
        row_number += 1
        ws.merge_cells(start_row=row_number, start_column=1, end_row=row_number, end_column=2)
        ws.append(item)
        for i in range(1, 6):
            ws.cell(row=row_number, column=i).border += borders['cart-items-table']

    row_number += 1
    ws.merge_cells(start_row=row_number, start_column=4, end_row=row_number, end_column=5)
    ws.append(['', '', u'Итог:', u'%s руб.' % order_info['total']])
    ws.cell(row=row_number, column=3).font = info_styles['font-order-info']
    ws.cell(row=row_number, column=4).alignment = info_styles['alignment-right']

    if discount:
        row_number += 1
        ws.merge_cells(start_row=row_number, start_column=4, end_row=row_number, end_column=5)
        ws.append(['', '', u'Итог (со скидкой %s %% ):' % (discount), u'%s руб.' % order_info['total_discount']])
        ws.cell(row=row_number, column=3).font = info_styles['font-order-info']
        ws.cell(row=row_number, column=4).alignment = info_styles['alignment-right']

    ws.column_dimensions["A"].width = 30.0
    ws.column_dimensions["B"].width = 20.0
    ws.column_dimensions["C"].width = 26.0
    ws.column_dimensions["D"].width = 11.0
    ws.column_dimensions["E"].width = 11.0

    ws.print_area = 'A1:E{}'.format(row_number)

    media_root = getattr(settings, 'MEDIA_ROOT')
    media_url = getattr(settings, 'MEDIA_URL')
    reports_root = os.path.join(media_root, 'reports')
    wb.save("%s/%s" % (reports_root, filename))

    return HttpResponseRedirect("%sreports/%s" % (media_url, filename))
