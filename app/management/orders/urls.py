# coding: utf-8
from django.conf.urls import url
from views import OrdersView, OrderManageView, OrderAccessError, print_order

urlpatterns = [
    url(r'^$', OrdersView.as_view(), name="index"),
    url(r'^order/(?P<order_id>[^/]+)/$', OrderManageView.as_view(), name="manage_order"),
    url(r'^order/(?P<order_id>[^/]+)/print/$', print_order, name="print_order"),
    url(r'access-error/$', OrderAccessError.as_view(), name="order_access_error"),
]
