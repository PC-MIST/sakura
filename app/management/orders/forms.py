# -*- coding: utf8 -*-
from app.order.models import Order
from django.forms.models import ModelForm


class ManageOrderForm(ModelForm):
    class Meta:
        model = Order
        fields = ['status', 'phone', 'delivery_location', 'persons', 'comment',
                  'first_name', 'last_name', 'rebate', 'total']
