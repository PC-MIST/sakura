# coding: utf-8
from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class OrdersManageConfig(AppConfig):
    name = 'app.management'
    verbose_name = _('Orders managing')
