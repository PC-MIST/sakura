# coding: utf-8
from django.conf.urls import url, include
from views import (IndexView, CallbacksView, RecallsView, RequestsView)

urlpatterns = [
    url(r'^$', IndexView.as_view(), name="index"),
    url(r'^orders/', include('app.management.orders.urls', namespace="orders")),
    url(r'^callbacks/$', CallbacksView.as_view(), name="callbacks"),
    url(r'^recalls/$', RecallsView.as_view(), name="recalls"),
    url(r'^requests/$', RequestsView.as_view(), name="requests"),
]
