from django.views.generic import TemplateView
from app.contrib.mixins import AppHeaderMixin


class RecallView(AppHeaderMixin, TemplateView):
    template_name = 'site/recall/index.html'
