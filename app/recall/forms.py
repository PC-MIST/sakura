from django import forms
from .models import Recall


class PostForm(forms.ModelForm):
    class Meta:
        model = Recall
        fields = ('recall', 'user',)
