# coding: utf-8
from django.conf.urls import url
from views import RecallView

urlpatterns = [
    url(r'^$', RecallView.as_view(app_header=u'Отзывы'), name="index"),
]
