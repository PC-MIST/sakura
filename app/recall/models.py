# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models


class Recall(models.Model):
    creation_date = models.DateTimeField(u"Creation date", auto_now_add=True)
    changing_date = models.DateTimeField(u"Changing date", auto_now=True)
    recall = models.TextField(verbose_name=u"Recall")
    user = models.OneToOneField(User, verbose_name=u"User", null=True)

    def __unicode__(self):
        return u"Отзыв %s." % self.id