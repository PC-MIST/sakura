# coding: utf-8
from django.views.generic import TemplateView
from app.contrib.mixins import AppHeaderMixin, StaticPageContentMixin


class AboutView(AppHeaderMixin, StaticPageContentMixin, TemplateView):
    template_name = 'site/about/index.html'
    page_slug = 'about'
