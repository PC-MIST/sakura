# coding: utf-8
from django.conf.urls import url
from views import AboutView

urlpatterns = [
    url(r'^$', AboutView.as_view(app_header=u'О нас'), name="index"),
]