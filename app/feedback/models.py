# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User

TYPE_LIST = (
    ("question", u"Вопрос"),
    ("error", u"Ошибка"),
    ("offer", u"Предложение"),
)


class Feedback(models.Model):
    creation_date = models.DateTimeField(u"Дата создания", auto_now_add=True)
    user = models.ForeignKey(User, verbose_name=u"Пользователь", blank=True, null=True)
    contact = models.CharField(verbose_name=u"Контакт", max_length=50)
    title = models.CharField(verbose_name=u"Заголовок", max_length=30)
    text = models.TextField(verbose_name=u"Текст сообщение")
    type = models.CharField(verbose_name=u"Тип сообщение",
                            choices=TYPE_LIST,
                            default=("question", u"Вопрос"), max_length=30)

    def __unicode__(self):
        return u"%s разработчикам. " % self.type
