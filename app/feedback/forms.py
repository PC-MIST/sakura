# coding: utf-8
from django import forms
from models import TYPE_LIST
from app.contrib.validators import contact
FORM_ATTRS = {'class': 'form-control'}


class FeedbackForm(forms.Form):
    title = forms.CharField(label=u'Заголовок', max_length=50,
                            widget=forms.TextInput(attrs=FORM_ATTRS))
    type = forms.ChoiceField(widget=forms.Select(attrs=FORM_ATTRS),
                             choices=TYPE_LIST,
                             label=u'Выберите тип сообщения')
    text = forms.CharField(label=u'Текст сообщения',
                           widget=forms.Textarea(attrs=FORM_ATTRS))
    contact = forms.CharField(label=u'Как с вами связаться?', required=False,
                              widget=forms.TextInput(attrs=FORM_ATTRS),
                              max_length=50, validators=[contact])
