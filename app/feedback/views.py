from django.views.generic import FormView
from forms import FeedbackForm
from models import Feedback


class FeedbackView(FormView):
    template_name = 'site/feedback/index.html'
    form_class = FeedbackForm
    success_url = '/'

    def form_valid(self, form):
        Feedback(**form.cleaned_data).save()
        return super(FeedbackView, self).form_valid(form)
