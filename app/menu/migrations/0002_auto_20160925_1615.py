# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('menu', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='product',
            options={'ordering': ['price'], 'verbose_name': '\u0442\u043e\u0432\u0430\u0440', 'verbose_name_plural': '\u0442\u043e\u0432\u0430\u0440\u044b'},
        ),
        migrations.AlterField(
            model_name='product',
            name='category',
            field=models.CharField(max_length=20, choices=[(b'sushi', '\u0421\u0443\u0448\u0438'), (b'rolls', '\u0420\u043e\u043b\u043b\u044b'), (b'tempura', '\u0422\u0435\u043c\u043f\u0443\u0440\u0430'), (b'sets', '\u041d\u0430\u0431\u043e\u0440\u044b'), (b'soups', '\u0421\u0443\u043f\u044b'), (b'souces', '\u0421\u043e\u0443\u0441\u044b'), (b'desserts', '\u0414\u0435\u0441\u0435\u0440\u0442\u044b')]),
        ),
    ]
