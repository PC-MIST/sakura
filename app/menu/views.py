from app.contrib.mixins import ProductCategoryMixin, AppHeaderMixin
from django.views.generic import TemplateView


class ProductView(ProductCategoryMixin, AppHeaderMixin, TemplateView):
    template_name = 'site/menu/index.html'