# -*- coding: utf-8 -*-
from django.conf.urls import url
from views import ProductView

urlpatterns = [
    url(r'^all/$', ProductView.as_view(category='all', app_header=u'Всё меню'),
        name="all"),
    url(r'^rolls/', ProductView.as_view(category='rolls', app_header=u'Роллы'),
        name="rolls"),
    url(r'^sushi/', ProductView.as_view(category='sushi', app_header=u'Суши'),
        name="sushi"),
    url(r'^tempura/',
        ProductView.as_view(category='tempura', app_header=u'Темпура'),
        name="tempura"),
    url(r'^sets/', ProductView.as_view(category='sets', app_header=u'Наборы'),
        name="sets"),
    url(r'^souces/', ProductView.as_view(category='souces', app_header=u'Соусы'),
        name="souces"),
    url(r'^hot/', ProductView.as_view(category='hot', app_header=u'Горячее'),
        name="hot"),
]
