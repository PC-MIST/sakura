# coding: utf-8
from django.db import models
from ckeditor.fields import RichTextField


class StaticPage(models.Model):
    page_title = models.CharField(verbose_name=u"Page title", max_length=50)
    page_slug = models.CharField(verbose_name=u"Page slug", max_length=50)
    html_text = RichTextField()
    creation_date = models.DateTimeField(u"Creation date", auto_now_add=True)
    update_date = models.DateTimeField(u"Update date", auto_now=True)

    class Meta:
        ordering = ['page_title']
        verbose_name = 'Статическая страница'
        verbose_name_plural = 'Статические страницы'

    def __unicode__(self):
        return u"%s" % self.page_title