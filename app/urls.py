# coding: utf-8
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    url(r'^', include('app.index.urls', namespace="index")),
    url(r'^admin/', admin.site.urls),
    url(r'^management/', include('app.management.urls', namespace="management")),
    url(r'^about/', include('app.about.urls', namespace="about")),
    url(r'^cart/', include('app.cart.urls', namespace="cart")),
    url(r'^contacts/', include('app.contacts.urls', namespace="contacts")),
    url(r'^feedback/', include('app.feedback.urls', namespace="feedback")),
    url(r'^menu/', include('app.menu.urls', namespace="menu")),
    url(r'^order/', include('app.order.urls', namespace="order")),
    url(r'^recall/', include('app.recall.urls', namespace="recall")),
    url(r'^shares/', include('app.shares.urls', namespace="shares")),
    url(r'^shipping_cond/', include('app.shipping_cond.urls',
                                    namespace="shipping_conditions")),
]
# urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
