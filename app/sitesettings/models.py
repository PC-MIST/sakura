# -*- coding: utf-8 -*-
from django.db import models


class SiteSetting(models.Model):
    name = models.CharField(verbose_name=u"Название настройки", max_length=256)
    slug = models.CharField(verbose_name=u"Slug", max_length=256)
    value = models.CharField(verbose_name=u"Значение", max_length=256)

    class Meta:
        ordering = ['name']
        verbose_name = 'настройка сайта'
        verbose_name_plural = 'настройки сайта'

    def __unicode__(self):
        return u"%s" % self.name
