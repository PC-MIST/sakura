# coding: utf-8
from .models import SiteSetting


def sitesettings_context(request):
    query = SiteSetting.objects.all()
    settings = {}
    for q in query:
        settings[q.slug] = q.value
    return settings