$(document).ready(function () {
    $('.scrollbar-dynamic').slimScroll({
        height: '155px'
    });

    var Product = function (id, name, price, weight, image, decr) {
        var self = this;
        self.id = ko.observable(id);
        self.name = ko.observable(name);
        self.price = ko.observable(price);
        self.weight = ko.observable(weight);
        self.image = ko.observable(image);
        self.description = ko.observable(decr);
    };

    var CartItem = function (product, quantity) {
        var self = this;
        self.product = ko.observable(product);
        self.quantity = ko.observable(quantity || 1);
        self.quantityTemp = ko.observable(quantity || 1); // Для хранения валидного значения
        self.quantityChanged = function () {
            if (self.quantity() < 1) {
                self.quantity(self.quantityTemp());
            }
            self.quantityTemp(self.quantity());
            var data = {
                'product': product.name(),
                'amount': self.quantity()
            };

            // Установление product.name() в self.quantity()
            API.setCartItem('', JSON.stringify(data), function (data) {
                if (data.result == 'error') {
                    self.quantity(self.quantityTemp());
                    console.log("Не удалось изменить количество продукта в корзине.")
                }
            });
        };

        self.cost = ko.computed(function () {
            return self.product().price() * self.quantity();
        });
    };

    var ViewModel = function () {
        var self = this;

        self.cart = ko.observableArray();
        self.products = ko.observableArray();
        self.productInfoModal = ko.observableArray();

        self.deliverCost = ko.observable(100.00);

        self.total = ko.computed(function () {
            var total = 0;
            $(self.cart()).each(function (index, cart_item) {
                total += cart_item.cost();
            });
            return total;
        });

        self.products_total = ko.computed(function () {
            var products_total = 0;
            $(self.cart()).each(function (index, cart_item) {
                products_total += cart_item.quantity();
            });
            return products_total;
        });

        self.addToCart = function (product, event) {
            var cart = self.cart();
            var hasItem = false; // Есть ли уже данный продукт в корзине
            var data = {
                'product': product.name()
            };
            // Поиск продукта в корзине с таким же именем.
            for (var i = 0; i < cart.length; i++) {
                if (cart[i].product().name() == product.name()) {
                    var quantity = cart[i].quantity() + 1;
                    cart[i].quantity(quantity);
                    hasItem = true;
                    data.amount = quantity;
                }
            }
            // Если нет такого же продукта в корзине, то добавляем этот продукт в корзину
            if (!hasItem) {
                var cart_item = new CartItem(product, 1);
                self.cart.push(cart_item);
                data.amount = 1;
            }

            API.setCartItem('', JSON.stringify(data), function (data) {
                if (data.result == 'error') {
                    console.log("Не удалось добавить продукт в корзину.")
                }
            });
        };

        self.addItemToCart = function (cart_item, event) {
            var amount = cart_item.quantity() + 1;
            var data = {
                'product': cart_item.product().name(),
                'amount': amount
            };

            API.setCartItem('', JSON.stringify(data), function (data) {
                if (data.result == 'error') {
                    console.log("Не удалось добавить товар в корзину.")
                }
            });
            cart_item.quantity(amount);
        };

        self.decItemFromCart = function (cart_item, event) {
            var amount = cart_item.quantity();
            var data = {
                'product': cart_item.product().name()
            };
            if (amount == 1) {
                API.delCartItem('', JSON.stringify(data), function (data) {
                    if (data.result == 'error') {
                        console.log("Не удалось удалить все продукты из корзины.")
                    }
                });
                self.cart.remove(cart_item);
                return;
            }
            amount -= 1;
            cart_item.quantity(amount);
            data.amount = amount;
            API.setCartItem('', JSON.stringify(data), function (data) {
                if (data.result == 'error') {
                    console.log("Не удалось убрать товар из корзины.")
                }
            });
        };


        self.removeFromCart = function (cart_item, event) {
            var data = {
                'product': cart_item.product().name()
            };
            API.delCartItem('', JSON.stringify(data), function (data) {
                if (data.result == 'error') {
                    console.log("Не удалось удалить все продукты из корзины.")
                }
            });
            self.cart.remove(cart_item);
        };

        self.showDescription = function (cart_item, event) {
            var name = cart_item.name(),
                price = cart_item.price(),
                weight = cart_item.weight(),
                imageURL = cart_item.image(),
                description = cart_item.description();

            console.log(cart_item);
            $('#product-description-modal-header').html(name);
            $('#product-description-modal-img').attr('src', imageURL);
            $('#product-description-modal-price').html('<strong>'+ price + ' руб.</strong>' + ' (' + weight + ' гр.)');
            $('#product-description-modal-description').html(description);
            $('#product-description-modal').modal({show: true});

            // var name = cart_item.name(),
            //     price = cart_item.price(),
            //     imageURL = cart_item.image(),
            //     description = cart_item.description();
            // bootbox.dialog({
            //     message: '<img src=' + imageURL + '  width="170px"/><br/>' +
            //     '<span style="font-family: Andalus; font-size: 16px;">' + description +
            //     '</span><br><br><span style="font-family: BrushScript; font-size: 16px;">'
            //     + price + 'руб.</span>',
            //     title: '<span style="font-family: SegoePrint; font-size: 22px;">' + name + '</span>',
            //     buttons: {
            //         success: {
            //             label: "В корзину",
            //             className: "btn-success",
            //             callback: function () {
            //                 self.addToCart(cart_item);
            //                 $('body').append(allert);
            //                 return false;
            //             }
            //         },
            //         close: {
            //             label: "Закрыть",
            //             className: "btn-danger",
            //             callback: function () {
            //                 return true;
            //             }
            //         }
            //     }
            // });
        };

        self.hideDescription = function () {
            console.log('ff22');
        };

        self.openCart = function () {
            $('#btn_cart_box_open').fadeOut();
            $('.cart-box').slideDown();
            // $("html, body").animate({scrollTop: 0}, 600);
        };

        self.closeCart = function () {
            $('.cart-box').slideUp();
            $('#btn_cart_box_open').fadeIn();
        };

        self.placeOrder = function () {
            window.location = "/order"
        }
    };

    window.view_model = new ViewModel();

    API.getCartItems('', function (data) {
        if (data.result == 'error') {
            return;
        }
        var data = JSON.parse(data.data);
        var cart_items = [];
        $.each(data, function (k, v) {
            var item = new Product(k, v['product'], v['price'], v['weight'], '/data/' + v['image']);
            cart_items.push(new CartItem(item, v['amount']));
        });
        view_model.cart(cart_items);
    });

    try {
        var category = $('#products')[0].getAttribute('data-bind-category');
        API.getProducts(category + '/', function (data) {
            var data = JSON.parse(data.data);
            var products = [];
            $.each(data, function (k, v) {
                products.push(new Product(k, v.name, v.price, v.weight, '/data/' + v.image, v.about))
            });
            view_model.products(products);
        });
    } catch (err) {

    }

    ko.applyBindings(window.view_model);
});
