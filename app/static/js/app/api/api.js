function createGetter(url) {
    return function (path_args, callback) {
        $.ajax({
            url: url + path_args,
            dataType: 'json',
//            data: options,
            success: callback
        });
    };
};

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');
function csrfSafeMethod(method) {
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

function createSetter(url) {
    return function (path_args, data, callback) {
        $.ajax({
            type: "POST",
            url: url + path_args,
            data: JSON.stringify(data),
            success: callback,
            contentType: "application/json",
            dataType: "json",
            crossDomain: false,
            beforeSend: function (xhr, settings) {
                if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                    xhr.setRequestHeader("X-CSRFToken", csrftoken);
                }
            }
        });
    };
};

API = {
    getCart: createGetter('/cart/api/get_cart/'),
    getCartItems: createGetter('/cart/api/get_cart_items/'),
    setCartItem: createSetter('/cart/api/set_cart_item/'),
    getProducts: createGetter('/cart/api/get_products/'),
    delCartItem: createSetter('/cart/api/del_cart_item/'),
};