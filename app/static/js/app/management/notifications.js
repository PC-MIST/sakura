$(document).ready(function () {
    var $orderNotifications = $('#order-notifications');
    var webSocketServerURL = $orderNotifications.attr('web-socket-server-url');
    var ws = new WebSocket(webSocketServerURL);
    ws.onopen = function () {
        $orderNotifications.hide();
    };
    ws.onmessage = function (ev) {
        $orderNotifications.fadeIn("slow");
        var data = JSON.parse(ev.data);
        var message = 'Поступил заказ(' + data.order_id + '). Клиент - ' + data.customer_name;
        var alert = '<div class="alert alert-success alert-dismissible fade in" role="alert">' +
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>' +
            '<strong>Внимание! </strong>' + message +
            '<br><a href="/management/orders/"><i class="fa fa-arrow-circle-left"></i> К заказам </a> '
        '</div>';
        $orderNotifications.append(alert);
        $.playSound("/static/sounds/management/arpeggio")
    };
    ws.onclose = function (ev) {
    };
    ws.onerror = function (ev) {
    };
});