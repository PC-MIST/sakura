# coding: utf-8
from django.conf.urls import url
from views import LoginFormView, LogoutView, IndexView
from app.menu.views import ProductView

urlpatterns = [
    url(r'^login/$', LoginFormView.as_view(), name="login"),
    url(r'^logout/$', LogoutView.as_view(), name="logout"),
    url(r'^$', ProductView.as_view(category='all', app_header=u"Всё меню"), name="home"),
]
