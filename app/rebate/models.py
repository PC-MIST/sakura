# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models


class Rebate(models.Model):
    class Meta:
        ordering = ['percent']
        verbose_name = u'скидка'
        verbose_name_plural = u'скидки'

    def __unicode__(self):
        return self.name

    name = models.CharField(verbose_name=u"Название", max_length=255)
    description = models.TextField(verbose_name=u"Описание", blank=True)
    is_enabled = models.BooleanField(verbose_name=u"Включено", default=True)
    percent = models.IntegerField(u'Процент скидки', default=0)
