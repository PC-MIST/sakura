# coding: utf-8
from django.conf.urls import url
from views import ContactsView

urlpatterns = [
    url(r'^$', ContactsView.as_view(app_header=u"Наши контакты"), name="index"),
]