# coding: utf-8
from django.views.generic import TemplateView
from app.contrib.mixins import AppHeaderMixin, StaticPageContentMixin


class ContactsView(AppHeaderMixin, StaticPageContentMixin, TemplateView):
    template_name = 'site/contacts/index.html'
    page_slug = 'contacts'
