from django.conf.urls import url
from views import UserView, SignUpView

urlpatterns = [
    url(r'^$', UserView.as_view(), name="profile"),
    url(r'^sign-up$', SignUpView.as_view(), name="sign_up"),
]
