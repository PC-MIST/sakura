# -*- coding: utf8 -*-
from django.views.generic import TemplateView


class UserView(TemplateView):
    template_name = 'site/index/user.html'


# https://habrahabr.ru/post/74165/
class SignUpView(TemplateView):
    template_name = 'site/user/sign_up.html'
