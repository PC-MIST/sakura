#!/usr/bin/env python
import sys

import os
from swampdragon.swampdragon_server import run_server

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'conf.dev.settings')
host_port = sys.argv[1] if len(sys.argv) > 1 else None
run_server(host_port=host_port)
