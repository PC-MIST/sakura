import os

APP_URL = "http://sushi-cakypa.ru"

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

PORT = 80

DEBUG = False
PRODUCTION = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sakura',
        'USER': 'sakura',
        'PASSWORD': 'NrAhc7Z7xHeKZNsB',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}
TIME_ZONE = 'Asia/Krasnoyarsk'

LANGUAGE_CODE = 'ru'
LANGUAGES = (
    ('ru', 'Russian'),
)
